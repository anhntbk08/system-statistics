'use strict';
angular.module('systemTrackingApp').config(function($stateProvider) {
  return $stateProvider.state('main', {
    url: '/',
    templateUrl: 'app/main/main.html',
    controller: 'MainCtrl'
  });
});
