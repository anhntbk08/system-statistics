
'use strict';
angular.module('systemTrackingApp').controller('PostgreStatisticCtrl', function($scope, $http, socket, DashboardServices) {
  var INTERVAL_POLL_TIME, MAXIMUM_TIME_CHART_POINTS, OFFSET_FOR_OLD_LOG, _toTimestamp, updatePollTime;
  INTERVAL_POLL_TIME = 5000;
  $scope.postgresqlDBInfo = {};

  updatePollTime = function() {
    var currentTime;
    currentTime = new Date();
    $scope.lastPoll = currentTime.toTimeString();
    return $scope.nextPoll = (new Date(currentTime.getTime() + INTERVAL_POLL_TIME)).toTimeString();
  };

  updatePollTime();

  $scope.databaseTablesInfo = [];
  $scope.connectionNumberDataset = [
    {
      label: 'Connection Number',
      color: '#6695C1',
      data: []
    }
  ];

  socket.onPostgresqlDBSizeChange(function(res) {
    if (!res.error) {
      $scope.dbSize = res.data[0].db_size;
      return res = null;
    }
  });

  socket.onPostgresqlTablesSizeChange(function(res) {
    if (!res.error) {
      $scope.databaseTablesInfo = res.data;
    }
    updatePollTime();
    return res = null;
  });

  socket.onConnectionNumberChange(function(res) {
    if (!res.error) {
      $scope.addDataToTimeChart(res);
    }
    return res = null;
  });

  DashboardServices.getPosgresqlDBInfo().then(function(res) {
    if (res.success) {
      $scope.postgresqlDBInfo = res.data;
    }
    return res = null;
  });

  _toTimestamp = function(dateText) {
    return new Date(dateText).getTime();
  };
  
  OFFSET_FOR_OLD_LOG = 20 * 1000;
  DashboardServices.getRecentPostgresLogs().then(function(res) {
    var latestLog, result;
    if (res.success) {
      result = [];
      latestLog = res.data[0];
      if ((new Date().getTime() - new Date(latestLog.timestamp).getTime()) > OFFSET_FOR_OLD_LOG) {
        return;
      }
      _.each(res.data, function(log) {
        return result.unshift([_toTimestamp(log.timestamp), JSON.parse(log.data).length]);
      });
      return $scope.connectionNumberDataset[0].data = result.splice(result.length - MAXIMUM_TIME_CHART_POINTS, MAXIMUM_TIME_CHART_POINTS);
    }
  });

  /*
    Add data to dataset each time getting new data from server
   */
  MAXIMUM_TIME_CHART_POINTS = 50;
  $scope.addDataToTimeChart = function(result) {
    var dataset;
    dataset = $scope.connectionNumberDataset[0].data;
    if (dataset.length < MAXIMUM_TIME_CHART_POINTS) {
      dataset.push([result.timestamp, result.data.length]);
    } else {
      dataset.shift();
      dataset.push([result.timestamp, result.data.length]);
    }
    return result = null;
  };
  return $scope.$on('$destroy', function() {
    return socket.unregisterPostgresqlEvents();
  });
});

