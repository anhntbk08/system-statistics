'use strict';
angular.module('systemTrackingApp')
  .controller('LogCtrl', function($scope, $http, socket, DashboardServices) {

    var LOG_ICONS = {
      'info': {
        icon: 'icon-plus',
        color: 'success',
      },
      'warn': {
        icon: 'icon-warning-sign',
        color: 'warning'
      },
      'error': {
        icon: 'icon-bolt',
        color: 'danger'
      },
      'fatal': {
        icon: 'icon-warning-sign',
        color: 'danger'
      },
      'debug': {
        icon: 'icon-time',
        color: 'default'
      },
      'trace': {
        icon: 'icon-bullhorn',
        color: 'info'
      }
    };

    $scope.logs = [];


    /*
      Get recently log
    */

    DashboardServices
      .getRecentDevelopmentLogs()
      .then(function(res){
        if (res.success){
          _.each(res.data, function(log){
            log.icon = LOG_ICONS[log.type];
          });

          $scope.logs = res.data;
          standardLogDate($scope.logs);
        }
      })

    // create listener development log
    socket
      .onGetNewDevelopmentLog(function(res) {
        // res.distanceFromNow = standardDate(res.time);
        res.icon = LOG_ICONS[res.type];
        $scope.logs.unshift(res);
        if ($scope.logs.length > 50)
          $scope.logs = $scope.logs.splice(0, 50);
        standardLogDate($scope.logs);

      });


    /*
      Show right date message for log, if happened date in
        -  < 60s:  Just now
        -  60s -> 1h: xxx minutes ago
        -  1h -> 1d: xxx hours ago
        -  1d: show date in dd mmm yyyy format
    */
    var standardLogDate = function(logs) {
      var result, distance;
      var currentDate = new Date();

      for (var index in logs){
        var date = new Date(logs[index].time || logs[index].createdAt);
      
        // if happens in 60s
        if (currentDate.valueOf() - date.valueOf() < 60000) {
          logs[index].distanceFromNow = "Just now !";
        } else

        // if  happens in 60s to 1 hours
        if (currentDate.valueOf() - date.valueOf() >= 60000 && currentDate.valueOf() - date.valueOf() < 3600000) {
          distance = parseInt((currentDate.valueOf() - date.valueOf()) / 60000);
          if (distance < 2) {
            logs[index].distanceFromNow =  distance + " minute ago !";
          } else {
            logs[index].distanceFromNow =  distance + " minutes ago !";
          }
        } else

        // if happens in range 1 hours to 1 day
        if (currentDate.valueOf() - date.valueOf() >= 3600000 && currentDate.valueOf() - date.valueOf() < 3600000 * 24) {
          distance = parseInt((currentDate.valueOf() - date.valueOf()) / 3600000);
          if (distance < 2) {
            logs[index].distanceFromNow =  distance + " hour ago !";
          } else {
            logs[index].distanceFromNow =  distance + " hours ago !";
          }
        } else 

        // if happens before 1 day
        {
          logs[index].distanceFromNow =  date.format("dd mmm yyyy");
        }
      }
      
    };

  });