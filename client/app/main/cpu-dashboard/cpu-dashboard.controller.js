'use strict';
angular.module('systemTrackingApp').controller('CPUCtrl', function($scope, $http, socket, DashboardServices) {
  /*
    Init statistic object
  */
  $scope.cpuInfo = {
    data: 0
  };
  $scope.ramInfo = {
    data: 0
  };
  $scope.diskInfo = {
    data: 0
  };
  $scope.networkIn = {
    data: 0
  };
  $scope.networkOut = {
    data: 0
  };

  // init dataset 
  var MAXIMUM_TIME_CHART_POINTS = 50;
  $scope.cpuDataset = [
    {
      label: 'CPU',
      color: '#6695C1',
      data: []
    }, {
      label: 'Memory',
      color: '#e25856',
      data: []
    }
  ];

  // type of cpu info
  var TYPES = {
    CPU: 0,
    MEMORY: 1,
    DISK: 2
  };

  // listener to handle cpu change event
  socket.onCPUChange(function(res) {
    if (!res.error) {
      //free up memory
      $scope.cpuInfo.data = null;

      // assign new 
      $scope.cpuInfo.data = parseFloat(res.result.data).toFixed(2);

      // add new data to real-time chart
      $scope.addDataToTimeChart(TYPES.CPU, [res.timestamp, $scope.cpuInfo.data]);

      //free up memory
      return res = null;
    }
  });

  // listener to handle memory change event
  socket.onMemoryChange(function(res) {
    if (!res.error) {
      $scope.ramInfo.data = parseFloat((1 - res.result.data.freemem / res.result.data.totalmem) * 100).toFixed(2);
      $scope.addDataToTimeChart(TYPES.MEMORY, [res.timestamp, $scope.ramInfo.data]);
      return res = null;
    }
  });

  // listener to handle disk change event
  socket.onDiskChange(function(res) {
    if (!res.error) {
      $scope.diskInfo.data = parseFloat(res.result.data.used / res.result.data.total * 100).toFixed(2);
      return res = null;
    }
  });

  // listener to network change event
  socket.onNetworkChange(function(res) {
    if (!res.error) {
      $scope.networkIn.data = res.result.data['eth0'].received_packets / 1000;
      $scope.networkOut.data = res.result.data['eth0'].sent_packets / 1000;
      return res = null;
    }
  });
  
  var _toTimestamp = function(dateText) {
    return new Date(dateText).getTime();
  };

  /*
    get recently logs for cpu statistic with offset 
  */
  var OFFSET_FOR_OLD_LOG = 20 * 1000;
  DashboardServices.getRecentCPULogs().then(function(res) {
    var cpuDataset, data, freePercentage, i, latestLog, len, log, memoryDataset, temp;
    if (res.data.length) {
      res = res.data;
      latestLog = res[0];

      if ((new Date().getTime() - new Date(latestLog.timestamp).getTime()) > OFFSET_FOR_OLD_LOG) {
        return;
      }

      cpuDataset = [];
      memoryDataset = [];
      for (i = 0, len = res.length; i < len; i++) {
        log = res[i];
        if (log.type === 'cpu_usage') {
          cpuDataset.push([_toTimestamp(log.timestamp), JSON.parse(log.data).usage]);
        } else if (log.type === 'memory_usage') {
          data = JSON.parse(log.data);
          freePercentage = (1 - data.freemem / data.totalmem) * 100;
          memoryDataset.push([_toTimestamp(log.timestamp), freePercentage]);
        }
      }
      temp = _.sortBy(cpuDataset, function(data) {
        return data[0];
      });
      $scope.cpuDataset[0].data = temp.splice(temp.length - MAXIMUM_TIME_CHART_POINTS, MAXIMUM_TIME_CHART_POINTS);
      temp = _.sortBy(memoryDataset, function(data) {
        return data[0];
      });
      $scope.cpuDataset[1].data = temp.splice(temp.length - MAXIMUM_TIME_CHART_POINTS, MAXIMUM_TIME_CHART_POINTS);
      res = null;
      return temp = null;
    }
  });

  /*
    Add data to dataset each time getting new data from server
   */
  $scope.addDataToTimeChart = function(dataType, data) {
    var dataset;
    if (!$scope.cpuDataset[dataType]) {
      return console.error("Please pass in right index !");
    }
    dataset = $scope.cpuDataset[dataType].data;
    if (dataset.length < MAXIMUM_TIME_CHART_POINTS) {
      dataset.push(data);
    } else {
      dataset.shift();
      dataset.push(data);
    }
    return data = null;
  };

  /*
    remove all listener when change page
  */
  return $scope.$on('$destroy', function() {
    return socket.unregisterCPUEvents();
  });
});
