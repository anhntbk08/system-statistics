'use strict';
describe('Controller: MainCtrl', function() {
  var $httpBackend, MainCtrl, scope;
  beforeEach(module('systemTrackingApp'));
  beforeEach(module('socketMock'));
  MainCtrl = void 0;
  scope = void 0;
  $httpBackend = void 0;
  beforeEach(inject(function(_$httpBackend_, $controller, $rootScope) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/things').respond(['HTML5 Boilerplate', 'AngularJS', 'Karma', 'Express']);
    scope = $rootScope.$new();
    return MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));
  return it('should attach a list of things to the scope', function() {
    $httpBackend.flush();
    return expect(scope.awesomeThings.length).toBe(4);
  });
});