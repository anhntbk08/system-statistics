'use strict';
angular.module('systemTrackingApp').factory('DashboardServices', function($http, $q) {
  return {
    /*
      Get recently cpu log
    */
    getRecentCPULogs: function() {
      var deferred;
      deferred = $q.defer();
      $http.get('/system/recentLogs').success(function(data) {
        return deferred.resolve(data);
      }).error(function(err) {
        return deferred.reject(err);
      });
      return deferred.promise;
    },

    /*
      Get basic information fo postgresql
    */
    getPosgresqlDBInfo: function() {
      var deferred;
      deferred = $q.defer();
      $http.get('/postgresql/dbInfo').success(function(data) {
        return deferred.resolve(data);
      }).error(function(err) {
        return deferred.reject(err);
      });
      return deferred.promise;
    },

    /*
      Get recent postgresql log
    */
    getRecentPostgresLogs: function() {
      var deferred;
      deferred = $q.defer();
      $http.get('/postgresql/recentConnectionLogs').success(function(data) {
        return deferred.resolve(data);
      }).error(function(err) {
        return deferred.reject(err);
      });
      return deferred.promise;
    },

    /*
      Get recent development log
    */
    getRecentDevelopmentLogs: function() {
      var deferred;
      deferred = $q.defer();
      $http.get('/dev_log/recentLogs').success(function(data) {
        return deferred.resolve(data);
      }).error(function(err) {
        return deferred.reject(err);
      });
      return deferred.promise;
    },
  };
});