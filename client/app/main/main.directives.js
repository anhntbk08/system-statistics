'use strict';
angular.module('systemTrackingApp')

/*
  Directive for pieChart
  @param {options}
*/
.directive('uiPieChart', [
  function() {
    return {
      restrict: 'EAM',
      replace: true,
      scope: {
        options: "="
      },
      link: function(scope, elem, attrs) {
        // call easyPieChart plugin
        elem.easyPieChart({
          size: 110,
          lineCap: 'butt',
          animate: 500,
          barColor: attrs.barColor || '#6695C1',
          lineWidth: 10
        });

        // watch the value change
        return scope.$watch("options.data", function(newVal, oldVal) {
          if (newVal) {
            return elem.data('easyPieChart').update(newVal);
          }
        });

      }
    };
  }
])

/*
  Directive for real-time chart
  @param {Array} dataset - list of dataset
*/
.directive('uiRealTimeChart', [
  function() {
    return {
      restrict: 'EAM',
      template: '<div></div>',
      replace: true,
      scope: {
        dataset: "="
      },
      link: function(scope, elem, attrs) {

        if (!scope.dataset) {
          return;
        }

        // set up options before drawing chart
        var options = {
          series: {
            lines: {
              show: true,
              fill: false
            }
          },
          xaxis: {
            mode: 'time',
            tickSize: [2, 'second'],
            tickFormatter: function(v, axis) {
              var date, hours, minutes, seconds;
              date = new Date(v);
              if (date.getSeconds() % 20 === 0) {
                hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
                minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
                seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
                return hours + ':' + minutes + ':' + seconds;
              } else {
                return '';
              }
            }
          },
          yaxis: {
            min: 0,
            max: 100
          }
        };

        // using plot plugin 
        scope.chart = $.plot($(elem), scope.dataset, options);

        // watch dataset change value
        return scope.$watch("dataset", function(newVal, oldVal) {
          if (newVal) {
            scope.chart = null;
            return scope.chart = $.plot($(elem), scope.dataset, options);
          }
        }, true);
      }
    };
  }
])

/*
  Directive for real-time chart
  @param {Array} dataset - list of dataset
*/
.directive('slimScroller', [
  function() {
    return {
      restrict: 'EAM',
      link: function(scope, elem, attrs) {

        $(elem).slimScroll({
          size: '7px',
          opacity: '0.2',
          position: 'right',
          height: $(elem).attr('data-height'),
          alwaysVisible: ($(elem).attr('data-always-visible') == '1' ? true : false),
          railVisible: ($(elem).attr('data-rail-visible') == '1' ? true : false),
          disableFadeOut: true
        });

      }
    };
  }
]);