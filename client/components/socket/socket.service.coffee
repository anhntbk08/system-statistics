# global io

'use strict'

angular.module 'systemTrackingApp'
.factory 'socket', (socketFactory) ->
  # socket.io now auto-configures its connection when we omit a connection url
  ioSocket = io '',
    # Send auth token on connection, you will need to DI the Auth service above
    # 'query': 'token=' + Auth.getToken()
    path: '/socket.io-client'

  socket = socketFactory ioSocket: ioSocket

  socket: socket

  onCPUChange: (callback)->
    socket.on 'system:cpu_usage', (res) =>
      callback? res

  onMemoryChange: (callback)->
    socket.on 'system:memory_usage', (res) =>
      callback? res

  onDiskChange: (callback)->
    socket.on 'system:disk_usage', (res) => 
      callback? res

  onNetworkChange: (callback)->
    socket.on 'system:network_usage', (res) =>
      callback? res

  onPostgresqlDBSizeChange: (callback)->
    socket.on 'postgresql:db_usage', (res) =>
      callback? res

  onPostgresqlTablesSizeChange: (callback)->
    socket.on 'postgresql:tables_usage', (res) =>
      callback? res

  onConnectToSystemChanel: (callback)-> 
    socket.on 'system:connect', (res) =>
      callback? res

  onConnectionNumberChange: (callback)-> 
    socket.on 'postgresql:connection_activities', (res) =>
      callback? res

  unregisterCPUEvents: ()->
    socket.removeAllListeners 'system:cpu_usage'
    socket.removeAllListeners 'system:memory_usage'
    socket.removeAllListeners 'system:disk_usage'
    socket.removeAllListeners 'system:network_usage'
    socket.removeAllListeners 'system:connect'

  unregisterPostgresqlEvents: ()->
    socket.removeAllListeners 'postgresql:db_usage'
    socket.removeAllListeners 'postgresql:tables_usage'

