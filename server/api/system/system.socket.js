/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var systemConfig   = require('../../config/environment'),
    cpu  = require('./components/cpu'),
    MonitorBase  = require('../base_socket/monitorBase'),
    memory  = require('./components/memory'),
    network  = require('./components/network'),
    harddrive  = require('./components/harddrive'),
    systemModel = require('./system.model'),
    util     = require('util'),
    _        = require('underscore'),
    INTERVAL_DELETE_LOG = 5*60*1000,
    defaults = {
      delay     : systemConfig.cpu_statistic.interval_time
    };

var SystemMonitor = MonitorBase.extend({
  init: function(socketIO, alias){
    alias = alias || 'system';
    this._super(socketIO, alias);
    console.log('start ...');
    this.start();
  },

  cycle: function(_target){
    _target.getCPUUsage();
    _target.getMemoryUsage();
    _target.getDiskUsage();
    _target.getNetworkUsage();

    // clear log last more than 5 mins
    _target.deleteLogsLastMoreThan(INTERVAL_DELETE_LOG);
  },
  
  getCPUUsage: function(){
    var self = this;
    cpu.cpuUsage(function(info){
      var usage = {result: {error: false, data: info }};
      addLog('cpu_usage', {usage: info});
      self.sendEvent('cpu_usage', usage);  
    });
  },

  getMemoryUsage: function(){
    var usage = {
                  result:
                    {
                      error: false,
                      data: {
                        freemem: memory.freemem(),
                        totalmem: memory.totalmem()
                      }
                    }
                };

    addLog('memory_usage', { freemem: memory.freemem(), totalmem: memory.totalmem() });

    this.sendEvent('memory_usage', usage);
  },

  getDiskUsage: function(){
    var self = this;
    harddrive.getDiskInfo(function(info){
      var usage = {
                    result:
                      {
                        error: false,
                        data: info
                      }
                  };
      addLog('disk_usage', info);
      self.sendEvent('disk_usage', usage);
    });
  },

  getNetworkUsage: function(){
    var self = this;
    network.getNetworkInfo(function(info){
      var usage = {
                    result:
                      {
                        error: false,
                        data: info
                      }
                  };
      addLog('network_usage', info);
      self.sendEvent('network_usage', usage);
    });
  },

  deleteLogsLastMoreThan: function(milliseconds){
    systemModel.deleteLogLastMoreThan(milliseconds);
  }
});

var addLog = function(type, data){

  systemModel.addLog({
    timestamp: _.now(),
    type: type,
    data: JSON.stringify(data)
  });
}



/*
  Using Singleton to ensure one interval-checking at a time.
  @socket Socket IO instance.
*/
var instance;
module.exports = {
  getInstance : function(socketIO, alias){
    if (!instance){
      instance = new SystemMonitor(socketIO, alias);
    } 
    return instance;
  }
};
