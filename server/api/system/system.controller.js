'use strict';

var systemModel = require('./system.model'),
    lodash = require('lodash');


/**
 * get past log a user
 * restriction: 'user'
 */
var LOGS_NUMBER = 250;
exports.getLogs = function(req, res) {
  systemModel.getLogsByQuantity(LOGS_NUMBER, function(result){
    result = result || [];
    res.json(200, {success: true, data: result});
  });
};
