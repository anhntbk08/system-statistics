var os = require('os'),
  child_process = require('child_process');

/*
  Get Processes running with those information
    - 'Command'
    - 'Count'
    - 'CPU %'
    - 'Memory %'
    - 'cpu'
    - 'mem'
*/
exports.getProcesses = function(callback) {
  if (!callback)
    throw new Error("Please pass in callback parameter !");
  
  var stats = {};
  var ps = child_process.exec('ps -ewwwo %cpu,%mem,comm', function(error, stdout, stderr) {
    var lines = stdout.split("\n");
    // Ditch the first line
    lines[0] = '';
    for (var line in lines) {
      var currentLine = lines[line].trim().replace('  ', ' ');
      //console.log(currentLine);
      var words = currentLine.split(" ");
      if (typeof words[0] !== 'undefined' && typeof words[1] !== 'undefined') {
        var cpu = words[0].replace(',', '.');
        var mem = words[1].replace(',', '.');
        var offset = cpu.length + mem.length + 2;
        var comm = currentLine.slice(offset);
        // If we're on Mac then remove the path
        if (/^darwin/.test(process.platform)) {
          comm = comm.split('/');
          comm = comm[comm.length - 1];
        } else {
          // Otherwise assume linux and remove the unnecessary /1 info like 
          // you get on kworker
          comm = comm.split('/');
          comm = comm[0];
        }
        // If already exists, then add them together
        if (typeof stats[comm] !== 'undefined') {
          stats[comm] = {
            cpu: parseFloat(stats[comm].cpu, 10) + parseFloat(cpu),
            mem: parseFloat(stats[comm].mem, 10) + parseFloat(mem),
            comm: comm,
            count: parseInt(stats[comm].count, 10) + 1
          };
        } else {
          stats[comm] = {
            cpu: cpu,
            mem: mem,
            comm: comm,
            count: 1
          };
        }
      }
    }
    var statsArray = [];
    for (var stat in stats) {
      // Divide by nuber of CPU cores
      var cpuRounded = parseFloat(stats[stat].cpu / os.cpus().length).toFixed(1);
      var memRounded = parseFloat(stats[stat].mem).toFixed(1);
      statsArray.push({
        'Command': stats[stat].comm,
        'Count': stats[stat].count,
        'CPU %': cpuRounded,
        'Memory %': memRounded,
        'cpu': stats[stat].cpu,
        'mem': stats[stat].mem // exact cpu for comparison
      });
    }
    statsArray.sort(function(a, b) {
      return parseFloat(b['cpu']) - parseFloat(a['cpu']);
    });

    callback(statsArray);

  });

}