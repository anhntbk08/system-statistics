var _os = require('os');
var fs = require('fs');

/*
  Get Network In/Out information
*/
exports.getNetworkInfo = function(callback) {
  if (!callback)
    throw new Error("Please pass in callback parameter !");

  return fs.readFile('/proc/net/dev', 'utf-8', function(err, meminfo) {
    if (err) throw err;
    var lines = meminfo.split("\n");
    var info = {};

    for (var i = 0; i < lines.length; i++) {
      var aline = lines[i].split(':');
      if (!aline[1]) continue;

      var key = aline.shift();
      key = key.replace(/ /g, "");
      aline = aline[0].replace(/\s+/g, ":");
      aline = aline.split(":");

      // return result
      info[key] = {
        received_bytes: aline[1],
        received_packets: aline[2],
        received_errors: aline[3],
        sent_bytes: aline[9],
        sent_packets: aline[10],
        sent_errors: aline[11]
      };
    }

    return callback(info);
  });
};