var _os = require('os');
var os_util = require('os-utils');
var fs = require('fs');

exports.platform = function() {
  return process.platform;
}

exports.cpuCount = function() {
  return _os.cpus().length;
}

exports.sysUptime = function() {
  //seconds
  return _os.uptime();
}

exports.processUptime = function() {
  //seconds
  return process.uptime();
}

/*
 * Returns All the load average usage for 1, 5 or 15 minutes.
 */
exports.allLoadavg = function() {

  var loads = _os.loadavg();

  return loads[0].toFixed(4) + ',' + loads[1].toFixed(4) + ',' + loads[2].toFixed(4);
}

/*
 * Returns the load average usage for 1, 5 or 15 minutes.
 */
exports.loadavg = function(_time) {

  if (_time === undefined || (_time !== 5 && _time !== 15)) _time = 1;

  var loads = _os.loadavg();
  var v = 0;
  if (_time == 1) v = loads[0];
  if (_time == 5) v = loads[1];
  if (_time == 15) v = loads[2];

  return v;
}

/*
    return the cpu free (in percentage) at the moment
*/
exports.cpuFree = function(callback) {
  if (!callback)
    throw new Error("Please pass in callback parameter !");

  os_util.cpuUsage(function(v) {
    callback(Math.floor((1 - v) * 100));
  });
}

/*
    return the cpu usage (in percentage) at the moment
*/
exports.cpuUsage = function(callback) {
  if (!callback)
    throw new Error("Please pass in callback parameter !");
  
  os_util.cpuUsage(function(v) {
    callback(Math.floor(v * 100));
  });
}