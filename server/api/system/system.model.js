'use strict';
var Sequelize = require('sequelize');

var SystemLog = sequelize.define('SystemLog', {
  type: Sequelize.STRING,
  timestamp: Sequelize.DATE,
  data: Sequelize.STRING(2000)  // json
});

module.exports = {
  getLogsFromDate: function(date){
    SystemLog.sync().then(function () {
      return SystemLog.findAll({
        where: {
          timestamp: {
            $gte: date
          }
        }
      });
    });
  },
  getLogsByQuantity: function(quantity, callback){ 
    SystemLog.sync().then(function () {
      SystemLog
        .findAll({
                  order: [['id', 'DESC']], 
                  where: {
                    type: {
                      $in: ['cpu_usage', 'memory_usage']
                    }  
                  },
                  limit: quantity })
        .then(function(logs){
          callback(logs);
        });
    });
  },
  addLog: function(log){
    SystemLog.sync().then(function () {
      return SystemLog.create(log);
    });
  },
  deleteLogLastMoreThan: function(offsetMillisecond){
    SystemLog.sync().then(function () {
      var date = new Date(new Date().getTime() - offsetMillisecond);

      return SystemLog.destroy({
        where: {
          timestamp: {
            $lt: date
          }
        }
      });
    });
  }
};