'use strict';

var express = require('express');
var controller = require('./dev_log.controller');

var router = express.Router();

router.get('/recentLogs', controller.getLogs);

module.exports = router;