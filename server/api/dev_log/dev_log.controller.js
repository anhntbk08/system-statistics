'use strict';

var devLogModel = require('../../components/logger/logger.model'),
    lodash = require('lodash');


/**
 * get past log a user
 * restriction: 'user'
 */
var LOGS_NUMBER = 20;
exports.getLogs = function(req, res) {
  devLogModel.getLogsByQuantity(LOGS_NUMBER, function(result){
    result = result || [];
    res.json(200, {success: true, data: result});
  });
};
