/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Class = require('../../components/baseClass'),
    util     = require('util'),
    _        = require('underscore'),
    defaults = {
      delay     : 1000
    };


/*
  Base class for system and postgresql statistic
  Class is based on John Resign Javascript inheristance
  Refer baseClass for more detail
*/
var Monitor = Class.extend({
  init: function(socketIO, alias){  
    this.socketIO = socketIO;
    this.alias = alias;
    this._monitorState = {
      running: false,
      ended: false,
      interval: undefined,
      config: _.clone(defaults)
    };
  },

  cycle : function(){
    throw new Error('Not Implemented !');
  },

  start: function() {
    var _self = this;
    if(_self._isEnded()) {
      throw new Error("monitor has been ended by .destroy() method");
    }

    /*
      we must pass a reference - _self instead of this object because
      inside setInterval -> javascript create new scope
    */
    process.nextTick(function(){
      _self.cycle(_self);
    });
    _self._monitorState.interval = setInterval(function(){
      _self.cycle(_self);
    }, _self._monitorState.config.delay);

    if(!_self.isRunning()) {
      _self._monitorState.running = true;
      _self.sendEvent('start', {type: 'start'});
    }

    // return this;
  },

  stop: function() {

    clearInterval(this._monitorState.interval);

    if(this.isRunning()) {
      this._monitorState.running = false;
      this.sendEvent('stop', {type: 'stop'});
    }

    return this;
  },

  reset: function() {
    this.sendEvent('reset', {type: 'reset'});
    this[this.isRunning() ? 'start' : 'config'](_.clone(defaults));
    return this;
  },

  destroy: function() {

    if(!this._isEnded()) {
      this.sendEvent('destroy', {type: 'destroy'});
      this.stop();
      this._monitorState.ended = true;
    }

    return this;
  },

  isRunning: function() {
    return !!this._monitorState.running;
  },

  _isEnded: function() {
    return !!this._monitorState.ended;
  },

  config: function(options) {
    if(_.isObject(options)) {
      _.extend(this._monitorState.config, options);
      this.sendEvent('config', {
                                 type: 'config',
                                 options: _.clone(options)
                               });
    }

    return this._monitorState.config;
  },

  sendEvent: function(event, obj) {

    var eventObject = _.extend({timestamp: _.now()}, obj);
    
    // for EventEmitter
    if (this.socketIO){
        this.socketIO.sockets.emit(this.alias + ":" + event, eventObject);
    }
   
  }
});


module.exports = Monitor;
