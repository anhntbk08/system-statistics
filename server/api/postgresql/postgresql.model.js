'use strict';
var Sequelize = require('sequelize');

var PostgresLog = sequelize.define('PostgresLog', {
  type: Sequelize.STRING,
  timestamp: Sequelize.DATE,
  data: Sequelize.TEXT // json
});

module.exports = {
  getLogsByQuantity: function(quantity, callback){ 
    PostgresLog.sync().then(function () {
      PostgresLog
        .findAll({
                  order: [['id', 'DESC']], 
                  where: {
                    type: {
                      $in: ['connection_activities']
                    }  
                  },
                  limit: quantity })
        .then(function(logs){
          callback(logs);
        });
    });
  },
  addConnectionLog: function(log){
    PostgresLog.sync().then(function () {
      return PostgresLog.create(log);
    });
  },
  deleteLogLastMoreThan: function(offsetMillisecond){
    PostgresLog.sync().then(function () {
      var date = new Date(new Date().getTime() - offsetMillisecond);

      return PostgresLog.destroy({
        where: {
          timestamp: {
            $lt: date
          }
        }
      });
    });
  }
};