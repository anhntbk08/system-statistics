'use strict';

var express = require('express');
var controller = require('./postgresql.controller');

var router = express.Router();

router.get('/recentConnectionLogs', controller.getConnectionLogs);
router.get('/dbInfo', controller.getDBInfo);

module.exports = router;