'use strict';

var postgresModel = require('./postgresql.model'),
    lodash = require('lodash'),
    systemConfig = require('../../config/environment'),
    os = require('os'),
    _ = require('lodash');


/**
 * get past log a user
 * restriction: 'user'
 */
var LOGS_NUMBER = 250;
exports.getConnectionLogs = function(req, res) {
  postgresModel.getLogsByQuantity(LOGS_NUMBER, function(result){
    result = result || [];
    res.json(200, {success: true, data: result});
  });
};


/**
 * get basic database information
 */
exports.getDBInfo = function(req, res) {
  var result = {
    os: {
      hostName: os.hostname(),
      platform: os.platform(),
      osType: os.type()
    },
    db: _.pick(systemConfig.postgresql, [
      "db_name",
      "port",
      "host"
    ])
  }
  res.json(200, {success: true, data: result});
};
