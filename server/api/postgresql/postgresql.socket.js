/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var systemConfig   = require('../../config/environment'),
    postgresqlModel = require('./postgresql.model'),
    db = require('./sqls/db'),
    tables = require('./sqls/tables'),
    activities = require('./sqls/activities'),
    MonitorBase  = require('../base_socket/monitorBase'),
    INTERVAL_DELETE_LOG = 5*60*1000,
    util     = require('util'),
    _        = require('underscore');


var PostgresqlMonitor = MonitorBase.extend({
  init: function(socketIO, sequelize, alias){
    alias = alias || 'postgresql';
    this.sequelize = sequelize;
    this._super(socketIO, alias);
    this._monitorState.config.delay = 5000;
    this.start();
  },

  cycle: function(_self){
    try{
      _self.getDBInfo();
      _self.getConnectionInfo();
      _self.getTablesInfo();
      _self.deleteLogsLastMoreThan(INTERVAL_DELETE_LOG);
    }
    catch (exception){
      _self.sendEvent('error', {exception: exception});
    }
  },

  getDBInfo: function(){
    var _self = this;
    db.getDBInfo(this.sequelize, function(data){
      _self.sendEvent('db_usage', {data: data });
    });
  },

  addConnectionLog: function(type, data){
    postgresqlModel.addConnectionLog({
      timestamp: _.now(),
      type: type,
      data: JSON.stringify(data)
    });
  },

  getConnectionInfo: function(){
    var _self = this;
    activities.getActivities(this.sequelize, function(data){
      _self.addConnectionLog('connection_activities', data);
      _self.sendEvent('connection_activities', {data: data});
    });
  },

  getTablesInfo: function(){
    var _self = this;
    tables.getTablesInfo(this.sequelize, function(data){
      _self.sendEvent('tables_usage', {data: data});
    });
  },

  deleteLogsLastMoreThan: function(milliseconds){
    postgresqlModel.deleteLogLastMoreThan(milliseconds);
  },

  throwException: function(exception){
    this.sendEvent('error', data);
  }
  
});


/*
  Using Singleton to ensure one interval-checking at a time.
  @socket Socket IO instance.
*/
var instance;
module.exports = {
  getInstance : function(socketIO, alias){
    if (!instance){
      instance = new PostgresqlMonitor(socketIO, alias);
    } 
    return instance;
  }
};
