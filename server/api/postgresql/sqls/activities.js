var systemConfig = require('../../../config/environment');

exports.getActivities = function(sequelize, callback) {
  if (!callback)
    throw new Error("Please pass in callback parameter !");
  
  var db = systemConfig.postgresql.db_name;
  var query = "select * from pg_stat_activity";

  sequelize
    .query(query, { type: sequelize.QueryTypes.SELECT})

    .then(function(activities) {
      if (!activities.length)
        throw new Error('The ' + db + ' schema is not defined yet !!!');
      callback(activities);
    })

    .catch(function(exception) {
      throw new Error({
        exception: exception,
        msg: 'Exception from excecuting sequelize !'
      });
    })
}