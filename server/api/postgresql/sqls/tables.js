var systemConfig = require('../../../config/environment');

exports.getTablesInfo = function(sequelize, callback) {
  if (!callback)
    throw new Error("Please pass in callback parameter !");

  var query = 'SELECT  relname as "Table", ' + 
                      'pg_size_pretty(pg_total_relation_size(relid)) As "Size", ' +
                      'pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) as "External Size" ' +
                      'FROM pg_catalog.pg_statio_user_tables ' +
                      'ORDER BY pg_total_relation_size(relid) DESC';

  sequelize
    .query(query, { type: sequelize.QueryTypes.SELECT})

    .then(function(tables_info) {
      callback(tables_info);
    })

    .catch(function(exception) {
      throw new Error({
        exception: exception,
        msg: 'Exception from excecuting sequelize !'
      });
    })
}