var systemConfig = require('../../../config/environment');

exports.getDBInfo = function(sequelize, callback) {
  if (!systemConfig.postgresql.db_name)
    throw new Error("Please set db_name in config/environment !");

  if (!callback)
    throw new Error("Please pass in callback parameter !");

  var db = systemConfig.postgresql.db_name;
  var query = "SELECT pg_database.datname, pg_size_pretty(pg_database_size(pg_database.datname)) as db_size " +
              "from pg_database " +
              "where pg_database.datname = '" + db + "'";

  sequelize
    .query(query, { type: sequelize.QueryTypes.SELECT})

    .then(function(db_info) {
      if (!db_info.length)
        throw new Error('The ' + db + ' schema is not defined yet !!!');

      callback(db_info);
    })

    .catch(function(exception) {
      throw new Error({
        exception: exception,
        msg: 'Exception from excecuting sequelize !'
      });
    })
}