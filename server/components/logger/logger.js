'use strict';

/*
  This module intend to override bunyan with
    -  log to db
    -  write log to socketio channel
*/

var Class       = require('./../baseClass.js'),
   bunyan       = require('bunyan'),
   loggerModel  = require('./logger.model'),
   _            = require('lodash'),
   ringbuffer   = new bunyan.RingBuffer({ limit: 1 });


// Overide bunyan log with writing to db and broadcast message to client
var Logger = Class.extend({
  init: function(socketIO, alias, showSource){
    this.socketIO = socketIO;
    this.alias = alias || 'logger';

    /*
      create logger, every log will be write to ringbuffer 
    */
    this.log = bunyan.createLogger({
      name: this.alias,
      src: showSource || false,
      streams: [{
        level:  'info',
        type:   'raw',
        stream: ringbuffer
      },{
        level:  'warn',
        type:   'raw',
        stream: ringbuffer
      },{
        level:  'error',
        type:   'raw',
        stream: ringbuffer
      },{
        level:  'fatal',
        type:   'raw',
        stream: ringbuffer
      },{
        level:  'debug',
        type:   'raw',
        stream: ringbuffer
      },{
        level:  'trace',
        type:   'raw',
        stream: ringbuffer
      }]
    });

    /*
      overide default functions of bunyan
    */
    var bunyanFns = ['info', 'warn', 'error', 'fatal', 'debug', 'trace'];

    // create logger function that equivalent to bunyan functions
    var _self = this;
    _.each(bunyanFns, function(fnName){
      _self[fnName] = function(){
        // transform arguments to array
        var args = Array.prototype.slice.call(arguments);

        // apply info function with args
        _self.log.info.apply(_self.log, args);

        // because ringbuffer length is maximum 1 so just get the first one
        // for latest log 
        var log = ringbuffer.records[0];

        // write to db
        _self.addLog(fnName, log.msg, JSON.stringify(log));

        log.type = fnName;

        // broadvast event to socketio channel
        _self.sendEvent('new_log', log);
      }
    });
    
  },

  // write log to db
  addLog: function(type, msg, metadata){
    loggerModel.addLog({
      msg: msg,
      type: type,
      metadata: metadata
    });
  },

  // broadcast event new log for client
  sendEvent: function(event, obj) {
    var eventObject = _.extend({timestamp: _.now()}, obj);
    if (this.socketIO){
        this.socketIO.sockets.emit(this.alias + ":" + event, eventObject);
    }
  }
  
});


var instance;
module.exports = {
  getInstance : function(socketIO, alias){
    if (!instance){
      instance = new Logger(socketIO, alias);
    } 
    return instance;
  }
};
