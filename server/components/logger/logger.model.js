'use strict';
var Sequelize = require('sequelize');

var DeveloperLog = sequelize.define('DeveloperLog', {
  type: Sequelize.STRING,
  msg: Sequelize.TEXT,
  metadata: Sequelize.TEXT
});

module.exports = {
  getLogsByQuantity: function(quantity, callback){ 
    DeveloperLog.sync().then(function () {
      DeveloperLog
        .findAll({
                  order: [['id', 'DESC']], 
                  limit: quantity
        })
        .then(function(logs){
          callback(logs);
        });
    });
  },
  addLog: function(log){
    DeveloperLog.sync().then(function () {
      return DeveloperLog.create(log);
    });
  }
};