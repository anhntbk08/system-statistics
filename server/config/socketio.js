/**
 * Socket.io configuration
 */

'use strict';

var config = require('./environment'),
    systemModel = require('../api/system/system.model'),
    _ = require('lodash');

// When the user disconnects.. perform this
function onDisconnect(socket) {
 
}

// When the user connects.. perform this
function onConnect(socket) {

  systemModel.getLogsByQuantity(250, function(prevLogs){
    socket.emit("system:connect", prevLogs);
  });
  
}

module.exports = function (socketio) {
  socketio.on('connection', function (socket) {
    socket.address = socket.handshake.address; 

    socket.connectedAt = new Date();

    // Call onDisconnect.
    socket.on('disconnect', function () {
      onDisconnect(socket);
      console.info(socket.address + 'DISCONNECTED');
    });

    // Call onConnect.
    onConnect(socket); 
    console.info('[%s] CONNECTED', socket.address);
  });
};