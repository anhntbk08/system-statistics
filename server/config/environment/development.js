'use strict';

// Development specific configuration
// ==================================
module.exports = {
  cpu_statistic: {
    interval_time: 1000
  },
  postgresql: { 
    db_name: 'system-statistic',
    interval_time: 5000,
    username: "postgres",
    password: "anhlavip",
    port: 5432,
    host: '192.168.48.128',
    dialect: 'postgres',
    logging: false
  },
  bunyan_log: {
    name: 'dev_log'
  }
};
