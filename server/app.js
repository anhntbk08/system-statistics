/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config/environment');

var Sequelize = require('sequelize');

// Connect to database
GLOBAL.sequelize = new Sequelize(config.postgresql.db_name,
  config.postgresql.username,
  config.postgresql.password, {
    host: config.postgresql.host,
    dialect: config.postgresql.dialect,
    logging: config.postgresql.logging
  });


// Setup server
var app = express();
var server = require('http').createServer(app);
var socketio = require('socket.io')(server, {
  serveClient: config.env !== 'production',
  path: '/socket.io-client'
});

require('./config/socketio')(socketio);
require('./config/express')(app);
require('./routes')(app);

// retrigger logger
GLOBAL.logger = require('./components/logger/logger').getInstance(socketio);

// start interval - writing system/postgresql log to db
require('./api/system/system.socket').getInstance(socketio);
require('./api/postgresql/postgresql.socket').getInstance(socketio, GLOBAL.sequelize);

// testing purpose
setInterval(function(){
  var arr = ['info', 'warn', 'error', 'fatal', 'debug', 'trace'];
  logger[arr[Math.round(Math.random()*5)]]('test ' + new Date());
  console.log('interval running');
}, 10000);


// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;
